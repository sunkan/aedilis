<?php

namespace Aedilis;

use Aura\Sql\ConnectionLocator;
use PDO;
use Psr\Cache\CacheItemPoolInterface;

class Access implements IConnection
{
    /**
     * @var Aura\Sql\ConnectionLocator
     */
    protected $connection_locator;

    /**
     * @var Psr\Cache\CacheItemPoolInterface
     */
    protected $cache;

    /**
     * @param ConnectionLocator $connection
     */
    public function setConnection(ConnectionLocator $connection)
    {
        $this->connection_locator = $connection;
    }

    /**
     * @param Psr\Cache\CacheItemPoolInterface $pool
     */
    public function setCachePool(CacheItemPoolInterface $pool)
    {
        $this->cache = $pool;
    }

    /**
     * @param array     $account
     * @return array
     */
    protected function getAccountAccess(array $account)
    {
        $account_id = $account['id'];
        $cacheItem = $this->cache->getItem(implode('/', [
            'account',
            $account_id,
            'access_list',
        ]));
        $list = $cacheItem->get();
        if (!$cacheItem->isHit()) {
            if (method_exists($cacheItem, 'lock')) {
                $cacheItem->lock();
            }
            $sql = "SELECT aa.key, aa.access FROM `aedilis_account_access` aa WHERE aa.account_id = :id
                        UNION
                    SELECT ra.key, ra.access FROM aedilis_account_roles ar
                        LEFT JOIN aedilis_role_access ra ON ar.role_id = ra.role_id
                        WHERE ar.account_id = :id";

            $stmt = $this->connection_locator->getRead()->prepare($sql);
            $stmt->execute([
                'id' => $account_id,
            ]);
            $rawAccess = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($rawAccess) === 0) {
                $accountRoleSql = 'INSERT INTO aedilis_account_roles (account_id, role_id) VALUES(:account_id, :role_id)';
                $this->connection_locator->getWrite()->perform($accountRoleSql, [
                    'account_id' => $account_id,
                    'role_id' => 1,
                ]);
                $stmt->execute([
                    'id' => $account_id,
                ]);
                $rawAccess = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }

            $list = [];
            foreach ($rawAccess as $access) {
                if (isset($list[$access['key']])) {
                    if ($access['access'] == 'denied') {
                        $list[$access['key']] = $access['access'];
                    }
                } else {
                    $list[$access['key']] = $access['access'];
                }
            }
            ;
            $this->cache->save($cacheItem->set($list));
        }
        return $list;
    }

    /**
     * @param array     $account
     * @param string    $key
     * @return bool
     */
    public function isAllowed(array $account, $key)
    {
        $list = $this->getAccountAccess($account);
        if ($list[$key] === 'approved') {
            return true;
        }
        if ($list['*'] === 'approved') {
            return true;
        }
        return false;
    }

    /**
     * @param array         $account
     * @param obj|array     $obj
     * @return mixed
     */
    public function isOwner(array $account, $obj)
    {
        if (is_array($obj)) {
            return $obj['account_id'] == $account['id'];
        } elseif (is_object($obj)) {
            return $obj->account_id = $account['id'];
        }
        return false;
    }
}

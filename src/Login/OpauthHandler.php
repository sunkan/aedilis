<?php

namespace Aedilis\Login;

use Aedilis\IConnection;
use Aedilis\Login as BaseLogin;
use Aura\Sql\ConnectionLocator;
use Exception;
use Opauth\Opauth\Opauth;
use Opauth\Opauth\OpauthException;
use PDO;

class OpauthHandler implements IHandler, IConnection
{
    /**
     * @var Aura\Sql\ConnectionLocator
     */
    protected $connection_locator;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param ConnectionLocator $connection
     */
    public function setConnection(ConnectionLocator $connection)
    {
        $this->connection_locator = $connection;
    }

    /**
     * @param Opauth\Opauth\Opauth $opauth
     */
    public function setOpauth(Opauth $opauth)
    {
        $this->opauth = $opauth;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $provider
     */
    public function handle($provider)
    {
        try {
            $response = $this->opauth->run();
        } catch (OpauthException $oe) {
            $msg = $oe->getMessage();
            if ($msg == 'Failed when attempting to authenticate account.') {
                return BaseLogin::FAIL;
            }
            throw new Exception($msg);
        }
        $connection = $this->connection_locator->getRead();
        if ($response['provider'] === 'aedilis') {
            $account = $connection->fetchOne('SELECT * FROM accounts WHERE id = :id', [
                'id' => $response['uid'],
            ]);
        } else {
            $sql = "SELECT a.*
                    FROM aedilis_oauths ao
                    LEFT JOIN accounts a ON a.id = ao.account_id
                    WHERE ao.provider = :provider AND ao.uid = :uid
                    LIMIT 1";

            $stmt = $connection->prepare($sql);
            $stmt->execute([
                'provider' => $response['provider'],
                'uid' => $response['uid'],
            ]);
            $account = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        if ($account) {
            $updateSql = "UPDATE aedilis_oauths ao
                            SET token = :token , expire = :expire, last_use = NOW()
                            WHERE uid = :uid AND provider = :provider
                            LIMIT 1";

            $updateStmt = $this->connection_locator->getWrite()->prepare($updateSql);
            $updateData = [
                'uid' => $response['uid'],
                'provider' => $response['provider'],
                'token' => $response['credentials']['token'],
                'expire' => $response['credentials']['expire'],
            ];

            $this->data = [
                'account' => $account,
            ];
            return BaseLogin::SUCCESS;
        } else {
            $this->data = [
                'credentials' => $response['credentials'],
                'provider' => strtolower($response['provider']),
                'uid' => $response['uid'],
                'raw' => $response['raw'],
            ];
            if (in_array($response['provider'], ['facebook', 'google'])) {
                return BaseLogin::REGISTRATION;
            }
            return BaseLogin::FAIL;
        }
    }
}

<?php

namespace Aedilis\Login;

interface IHandler
{
    public function handle($provider);
}

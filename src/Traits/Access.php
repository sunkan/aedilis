<?php

namespace Aedilis\Traits;

use Aedilis\Access as AccessHandler;
use RuntimeException;

trait Access
{
    /**
     * @var Aedilis\Access
     */
    protected $aedilis_access;

    /**
     * @param Aedilis\Access $handler
     */
    public function setAccessHandler(AccessHandler $handler)
    {
        $this->aedilis_access = $handler;
    }

    /**
     * @param string    $key
     * @return bool
     */
    public function isAllowed($key)
    {
        if (!method_exists($this, 'hasAuth')) {
            throw new RuntimeException("Aedilis\Traits\Access need Aedilis\Traits\Auth");
        }
        if (!$this->hasAuth()) {
            return false;
        }
        return $this->aedilis_access->isAllowed($this->getAccount(), $key);
    }

    /**
     * @param $obj
     * @return bool
     */
    public function isOwner($obj)
    {
        if (!method_exists($this, 'hasAuth')) {
            throw new RuntimeException("Aedilis\Traits\Access need Aedilis\Traits\Auth");
        }
        if (!$this->hasAuth()) {
            return false;
        }
        return $this->aedilis_access->isOwner($this->getAccount(), $obj);
    }
}

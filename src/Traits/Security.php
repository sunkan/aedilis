<?php

namespace Aedilis\Traits;

trait Security
{
    use Auth, Access;
}

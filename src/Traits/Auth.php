<?php

namespace Aedilis\Traits;

use Aedilis\Auth as AuthHandler;

trait Auth
{
    /**
     * @var Aedilis\Auth
     */
    protected $aedilis_auth;

    /**
     * @var bool
     */
    private $__has_auth;

    /**
     * @param Aedilis\Auth $handler
     */
    public function setAuthHandler(AuthHandler $handler)
    {
        $this->aedilis_auth = $handler;
    }

    /**
     * @return bool
     */
    public function hasAuth()
    {
        if (!isset($this->__has_auth)) {
            $this->__has_auth = $this->aedilis_auth->hasAuth();
        }
        return $this->__has_auth;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        if ($this->__has_auth || $this->hasAuth()) {
            return $this->aedilis_auth->account;
        }
        return false;
    }
}

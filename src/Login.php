<?php

namespace Aedilis;

use Aura\Session\Session;

class Login
{
    const SUCCESS = 1;
    const FAIL = 2;
    const REGISTRATION = 3;

    /**
     * @var Aedilis\Login\IHandler
     */
    protected $handler;

    /**
     * @var Aura\Session\Session
     */
    protected $session;

    /**
     * @param Aedilis\Login\IHandler $handler
     */
    public function __construct(Login\IHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param Aura\Session\Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session->getSegment('Aedilis\Auth');
    }

    /**
     * @param Aedilis\Login\IHandler $handler
     */
    public function setHandler(Login\IHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param $provider
     * @return mixed
     */
    public function __invoke($provider = '')
    {
        return $this->handler->handle($provider);
    }

    /**
     * @param array|null    $account
     */
    public function persist($account = null)
    {
        if ($account === null) {
            $account = $this->data['account'];
        }

        if ($account) {
            $this->session->set('account', $account);
        }
    }

    /**
     * @param string    $key
     * @return mixed
     */
    public function __get($key)
    {
        if ($key === 'handler') {
            return $this->handler;
        }
        if ($key === 'provider') {
            $data = $this->handler->getData();
            return $data['provider'];
        }
        if ($key === 'data') {
            return $this->handler->getData();
        }
    }
}

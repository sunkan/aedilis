<?php

namespace Aedilis\Admin;

use Aedilis\IConnection;
use Aura\Sql\ConnectionLocator;
use PDO;

class Role implements IConnection
{
    /**
     * @var Aura\Sql\ConnectionLocator
     */
    protected $connection_locator;

    /**
     * @var array
     */
    protected $list;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
    }

    /**
     * @param ConnectionLocator $connection
     */
    public function setConnection(ConnectionLocator $connection)
    {
        $this->connection_locator = $connection;
    }

    /**
     * @return array
     */
    public function getList()
    {
        if ($this->list) {
            return $this->list;
        }
        $sql = "SELECT ra.key, ra.access FROM aedilis_role_access ra
                WHERE ra.role_id = :id";

        $stmt = $this->connection_locator->getRead()->prepare($sql);
        $stmt->execute([
            'id' => $this->id,
        ]);
        $rawAccess = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $list = [];
        foreach ($rawAccess as $access) {
            if (isset($list[$access['key']])) {
                if ($access['access'] === 'denied') {
                    $list[$access['key']] = $access['access'];
                }
            } else {
                $list[$access['key']] = $access['access'];
            }
        }
        return $this->list = $list;
    }

    /**
     * @param string    $key
     * @param string    $access
     * @return bool
     */
    public function add($key, $access = 'approved')
    {
        if (!in_array($access, ['approved', 'denied'])) {
            return false;
        }

        $sql = 'INSERT INTO aedilis_role_access (`role_id`, `key`, `access`) VALUES(:role_id, :key, :access)';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);

        return $stmt->execute([
            'role_id' => $this->id,
            'key' => $key,
            'access' => $access,
        ]);
    }

    /**
     * @param string    $key
     * @return bool
     */
    public function remove($key)
    {
        $sql = 'DELETE aedilis_role_access WHERE `role_id` = :role_id AND `key` = :key LIMIT 1';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);

        return $stmt->execute([
            'role_id' => $this->id,
            'key' => $key,
        ]);
    }

    /**
     * @param string    $key
     * @param string    $access
     * @return bool
     */
    public function change($key, $access)
    {
        if (!in_array($access, ['approved', 'denied'])) {
            return false;
        }
        $sql = 'UPDATE aedilis_role_access SET access = :access WHERE `role_id` = :role_id AND `key` = :key LIMIT 1';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);

        return $stmt->execute([
            'role_id' => $this->id,
            'key' => $key,
            'access' => $access,
        ]);
    }
}

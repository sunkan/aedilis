<?php

namespace Aedilis\Admin;

use Aedilis\IConnection;
use Aura\Sql\ConnectionLocator;

class Access implements IConnection
{
    /**
     * @var Aura\Sql\ConnectionLocator
     */
    protected $connection_locator;

    /**
     * @param Aura\Sql\ConnectionLocator $connection
     */
    public function setConnection(ConnectionLocator $connection)
    {
        $this->connection_locator = $connection;
    }

    /**
     * @return array<Aedilis\Admin\Role>
     */
    public function getRoles()
    {
        $sql = 'SELECT * FROM aedilis_roles';
        $stmt = $this->connection_locator->getRead()->prepare($sql);
        $stmt->execute();
        $roles = [];

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $role = new Role($row);
            $roles[] = $role;
        }
        return $roles;
    }

    /**
     * @param int $id
     * @return Aedilis\Admin\Role
     */
    public function getRole($id)
    {
        $sql = 'SELECT * FROM aedilis_roles WHERE id = :id';
        $stmt = $this->connection_locator->getRead()->prepare($sql);
        $stmt->execute([
            'id' => $id,
        ]);

        return new Role($stmt->fetch(PDO::FETCH_ASSOC));
    }

    /**
     * @param Role $role
     * @return bool
     */
    public function deleteRole(Role $role)
    {
        $sql = 'DELETE FROM aedilis_roles WHERE id = :id';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);
        return $stmt->execute([
            'id' => $role->id,
        ]);
    }

    /**
     * @param string    $name
     * @return Aedilis\Admin\Role|false
     */
    public function createRole($name)
    {
        $sql = 'INSERT INTO aedilis_roles (name) VALUE(:name)';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);
        $rs = $stmt->execute([
            'name' => $name,
        ]);
        if ($rs) {
            $id = $this->connection_locator->getWrite()->lastInsertId();
            return new Role([
                'id' => $id,
                'name' => $name,
            ]);
        }
        return false;
    }

    /**
     * @param int $id
     * @return Aedilis\Admin\Account
     */
    public function getAccount($id)
    {
        $sql = 'SELECT * FROM accounts WHERE id = :id';
        $stmt = $this->connection_locator->getRead()->prepare($sql);
        $stmt->execute([
            'id' => $id,
        ]);

        return new Account($stmt->fetch(PDO::FETCH_ASSOC));
    }
}

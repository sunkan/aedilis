<?php

namespace Aedilis\Admin;

use Aedilis\IConnection;
use Aura\Sql\ConnectionLocator;
use PDO;

class Account implements IConnection
{
    /**
     * @var Aura\Sql\ConnectionLocator
     */
    protected $connection_locator;

    /**
     * @var array
     */
    protected $list;

    /**
     * @var int
     */
    public $id = false;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->id = $data['id'];
    }

    /**
     * @param ConnectionLocator $connection
     */
    public function setConnection(ConnectionLocator $connection)
    {
        $this->connection_locator = $connection;
    }

    /**
     * @return array
     */
    public function getList()
    {
        if ($this->list) {
            return $this->list;
        }
        $sql = "SELECT aa.key, aa.access FROM `aedilis_account_access` aa WHERE aa.account_id = :id
                    UNION
                SELECT ra.key, ra.access FROM aedilis_account_roles ar
                    LEFT JOIN aedilis_role_access ra ON ar.role_id = ra.role_id
                    WHERE ar.account_id = :id";

        $stmt = $this->connection_locator->getRead()->prepare($sql);
        $stmt->execute([
            'id' => $this->id,
        ]);
        $rawAccess = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $list = [];
        foreach ($rawAccess as $access) {
            if (isset($list[$access['key']])) {
                if ($access['access'] == 'denied') {
                    $list[$access['key']] = $access['access'];
                }
            } else {
                $list[$access['key']] = $access['access'];
            }
        }
        return $this->list = $list;
    }

    /**
     * @param Aedilis\Admin\Role $role
     * @return bool
     */
    public function addRole(Role $role)
    {
        $sql = 'INSERT INTO aedilis_account_roles (account_id, role_id) VALUES(:account_id, :role_id)';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);
        return $stmt->execute([
            'account_id' => $this->id,
            'role_id' => $role->id,
        ]);
    }

    /**
     * @param Aedilis\Admin\Role|int    $role
     * @return bool
     */
    public function removeRole($role)
    {
        if ($role instanceof Role) {
            $role_id = $role_id;
        } elseif (is_int($role)) {
            $role_id = $role;
        } else {
            return false;
        }

        $sql = 'DELETE FROM aedilis_account_roles WHERE role_id = :role_id AND account_id = :account_id LIMIT 1';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);
        return $stmt->execute([
            'account_id' => $this->id,
            'role_id' => $role_id,
        ]);
    }

    /**
     * @param string    $key
     * @param string    $access
     * @return bool
     */
    public function add($key, $access = 'approved')
    {
        if (!in_array($access, ['approved', 'denied'])) {
            return false;
        }

        $sql = 'INSERT INTO aedilis_account_access (`account_id`, `key`, `access`) VALUES(:account_id, :key, :access)';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);

        return $stmt->execute([
            'account_id' => $this->id,
            'key' => $key,
            'access' => $access,
        ]);
    }

    /**
     * @param string    $key
     * @return bool
     */
    public function remove($key)
    {
        $sql = 'DELETE aedilis_account_access WHERE `account_id` = :account_id AND `key` = :key LIMIT 1';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);

        return $stmt->execute([
            'account_id' => $this->id,
            'key' => $key,
        ]);
    }

    /**
     * @param string    $key
     * @param string    $access
     * @return bool
     */
    public function change($key, $access)
    {
        if (!in_array($access, ['approved', 'denied'])) {
            return false;
        }
        $sql = 'UPDATE aedilis_account_access SET access = :access WHERE `account_id` = :account_id AND `key` = :key LIMIT 1';
        $stmt = $this->connection_locator->getWrite()->prepare($sql);

        return $stmt->execute([
            'account_id' => $this->id,
            'key' => $key,
            'access' => $access,
        ]);
    }
}

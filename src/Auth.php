<?php

namespace Aedilis;

use Aura\Session\Session;
use Psr\Cache\CacheItemPoolInterface;

class Auth
{
    /**
     * @var Aura\Session\Session
     */
    protected $session;

    /**
     * @var Psr\Cache\CacheItemPoolInterface
     */
    protected $cache;

    /**
     * @var array
     */
    protected $account;

    /**
     * @var bool
     */
    protected $hasAuth = false;

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session->getSegment('Aedilis\Auth');
    }

    /**
     * @param Psr\Cache\CacheItemPoolInterface $pool
     */
    public function setCachePool(CacheItemPoolInterface $pool)
    {
        $this->cache = $pool;
    }

    /**
     * @param string    $key
     * @return mixed
     */
    public function __get($key)
    {
        if ($key == 'account') {
            return $this->account;
        }
    }

    /**
     * @return bool
     */
    public function clearCache()
    {
        if (!$this->hasAuth()) {
            return false;
        }
        if ($this->cache) {
            $this->cache->deleteItem(implode('/', [
                'account',
                $this->account->id,
            ]));
        }
        return true;
    }

    /**
     * @return bool
     */
    public function hasAuth()
    {
        if (!isset($this->account)) {
            $this->account = $this->session->get('account');
            $this->hasAuth = false;
            if ($this->account) {
                $this->hasAuth = true;
            }
        }
        return $this->hasAuth;
    }
}

<?php

namespace Aedilis;

use Aura\Sql\ConnectionLocator;

interface IConnection
{
    /**
     * @param ConnectionLocator $connection
     */
    public function setConnection(ConnectionLocator $connection);
}

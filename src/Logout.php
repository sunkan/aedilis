<?php

namespace Aedilis;

use Aura\Session\Segment;
use Aura\Session\Session;
use Psr\Cache\CacheItemPoolInterface;

class Logout
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Segment
     */
    protected $session_segment;

    /**
     * @var  Psr\Cache\CacheItemPoolInterface
     */
    protected $cache;

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session;
        $this->session_segment = $session->getSegment('Aedilis\Auth');
    }

    /**
     * @param Psr\Cache\CacheItemPoolInterface $pool
     */
    public function setCachePool(CacheItemPoolInterface $pool)
    {
        $this->cache = $pool;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $account = $this->session->get('account');
        if ($account && $this->cache) {
            $this->cache->deleteItem(implode('/', [
                'account',
                $account['id'],
            ]));
        }

        $this->session_segment->clear();
        return $this->session->regenerateId();
    }
}

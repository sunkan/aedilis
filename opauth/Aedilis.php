<?php

namespace Opauth\Aedilis\Strategy;

use Aedilis\IConnection;
use Aura\Sql\ConnectionLocator;
use Opauth\Opauth\AbstractStrategy;
use PDO;

class Aedilis extends AbstractStrategy implements IConnection
{
    protected $responseMap = [
        'name' => 'info.name',
        'uid' => 'id',
        'info.name' => 'info.name',
        'info.email' => 'email',
    ];

    protected $connection_locator;

    public function setConnection(ConnectionLocator $connection)
    {
        $this->connection_locator = $connection;
    }

    public function callback()
    {
        //Response attributes $uid, $name and $credentials MUST be set.
        $results = $this->sessionData();
        if (isset($results['error'])) {
            $this->error($results['error']['message'], $results['error']['code'], $results);
        }
        $response = $this->response($results);
        $response->credentials = array(
            'token' => $results['access_token'],
            'expires' => isset($results['expires']) ? date('c', time() + $results['expires']) : null,
        );
        $response->map();
        return $response;
    }

    public function request()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $sql = 'SELECT a.* FROM accounts a WHERE email LIKE :email LIMIT 1';
        $stmt = $this->connection_locator->getRead()->prepare($sql);
        $rs = $stmt->execute([
            'email' => $email,
        ]);
        $account = $stmt->fetch(PDO::FETCH_ASSOC);
        if (password_verify($password, $account['password'])) {
            $data = [
                'id' => $account['id'],
                'email' => $account['email'],
                'info' => [
                    'name' => $account['email'],
                    'country' => $account['country'],
                ],
            ];
            $data['access_token'] = sha1(time() . serialize($account));
        } else {
            $data = [
                'error' => [
                    'message' => 'Failed when attempting to authenticate account.',
                    'code' => 'access_denied',
                ],
            ];
        }

        $this->sessionData($data);

        $this->redirect($this->callbackUrl());
    }
}

-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: 109.74.15.132
-- Generation Time: Nov 04, 2015 at 11:53 AM
-- Server version: 5.6.11-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `stylewish`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_access`
--

CREATE TABLE IF NOT EXISTS `aedilis_account_access` (
  `account_id` int(10) unsigned NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `access` enum('approved','denied') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `account_roles`
--

CREATE TABLE IF NOT EXISTS `aedilis_account_roles` (
  `account_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifyed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_roles`
--

CREATE TABLE IF NOT EXISTS `aedilis_roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_role_access`
--

CREATE TABLE IF NOT EXISTS `aedilis_role_access` (
  `role_id` int(10) unsigned NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `access` enum('approved','denied') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'approved'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `account_oauths`
--

CREATE TABLE IF NOT EXISTS `aedilis_oauths` (
  `id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `provider` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `uid` text COLLATE utf8_unicode_ci NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `secret` text COLLATE utf8_unicode_ci NOT NULL,
  `last_use` datetime NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_access`
--
ALTER TABLE `aedilis_account_access`
  ADD PRIMARY KEY (`account_id`,`key`);

--
-- Indexes for table `account_roles`
--
ALTER TABLE `aedilis_account_roles`
  ADD PRIMARY KEY (`account_id`,`role_id`);

--
-- Indexes for table `auth_roles`
--
ALTER TABLE `aedilis_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_role_access`
--
ALTER TABLE `aedilis_role_access`
  ADD PRIMARY KEY (`role_id`,`key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_roles`
--
ALTER TABLE `aedilis_roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

--
-- Indexes for table `account_oauths`
--
ALTER TABLE `aedilis_oauths`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_id` (`account_id`,`provider`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_oauths`
--
ALTER TABLE `aedilis_oauths`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;


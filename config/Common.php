<?php

namespace Aedilis\_Config;

use Aura\Di\Config;
use Aura\Di\Container;

class Common extends Config
{
    const VERSION = '2.2.0';

    public function define(Container $di)
    {
        $di->setter['Aedilis\IConnection']['setConnection'] = $di->lazyGet('connection');
        $di->setter['Aedilis\Auth']['setSession'] = $di->lazyGet('session');
        $di->setter['Aedilis\Login']['setSession'] = $di->lazyGet('session');
        $di->setter['Aedilis\Logout']['setSession'] = $di->lazyGet('session');
        $di->setter['Aedilis\Login\OpauthHandler']['setOpauth'] = $di->lazyGet('opauth');

        if ($di->has('cache_pool')) {
            $di->setter['Aedilis\Auth']['setCachePool'] = $di->lazyGet('cache_pool');
            $di->setter['Aedilis\Access']['setCachePool'] = $di->lazyGet('cache_pool');
            $di->setter['Aedilis\Logout']['setCachePool'] = $di->lazyGet('cache_pool');
        }

        $di->setter['Aedilis\Traits\Access']['setAccessHandler'] = $di->lazyNew('Aedilis\Access');
        $di->setter['Aedilis\Traits\Auth']['setAuthHandler'] = $di->lazyNew('Aedilis\Auth');
    }
}
